FROM alpine:latest
ADD ./caddy /caddy
RUN mkdir /config
RUN mkdir /data
ENV XDG_CONFIG_HOME=/config
ENV XDG_DATA_HOME=/data
EXPOSE 80
EXPOSE 443
EXPOSE 443/udp
EXPOSE 2019
ENTRYPOINT ["/caddy"]
CMD  ["run", "--config", "/Caddyfile"]
